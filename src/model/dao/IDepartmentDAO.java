package model.dao;

import java.util.List;

import model.entities.Department;

public interface IDepartmentDAO {
	
	public void agregar(Department d);
	
	public void modificar(Department d, String identificador);
	
	public void eliminar(Department d);
	
	public void recuperar(Department d);
	
	public List<Department> buscarTodos();
	
	public List<Department> buscarEliminados();

}
