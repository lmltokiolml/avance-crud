package model.dao;

import java.util.List;

import model.entities.Course;

public interface ICourseDAO {

	public void agregar(Course co);
	
	public void modificar(Course co, String identificador);
	
	public void eliminar(Course co);
	
	public void recuperar(Course co);
	
	public List<Course> buscarTodos();
	
	public List<Course> buscarEliminados();
}
