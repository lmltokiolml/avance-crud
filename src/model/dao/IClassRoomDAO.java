package model.dao;

import java.util.List;

import model.entities.Classroom;



public interface IClassRoomDAO {
	
	
public void agregar(Classroom cla);
	
	public void modificar(Classroom cla, String identificador);
	
	public void eliminar(Classroom cla);
	
	public void recuperar(Classroom cla);
	
	public List<Classroom> buscarTodos();
	
	public List<Classroom> buscarEliminados();


	
}
