package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.database.MysqlConnect;
import model.entities.Classroom;


public class ClassRoomDAO implements IClassRoomDAO{
	
	MysqlConnect con;
	private String getbuilding;
	
	public ClassRoomDAO(){
		con = new MysqlConnect();
		
		con.connect();
	}

	@Override
	public void agregar(Classroom cla) {
		
			// TODO Auto-generated method stub
			try {
				PreparedStatement statement = con.getCon().prepareStatement("INSERT INTO classroom"
																			+ "(building,room_number,capacity)" 
																			+ "VALUES(?,?,?)");
				statement.setString(1, cla.getBuilding());
				statement.setString(2, cla.getRoom_number());
				
				/*String departmentId = co.getDepartment().getDept_name();
	            
	            PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
	            statement2.setString(1, departmentId);
	            
	            ResultSet deptRS = statement2.executeQuery();
	            
	            Department d = new Department();
	            d.setDept_name(departmentId);
	            
	            while(deptRS.next()) {
	            	d.setBuilding(deptRS.getString("building"));
	            	d.setDept_name(deptRS.getString("dept_name"));
	            	d.setBudget(deptRS.getInt("budget"));
	            }

				statement.setString(3, d.getDept_name());*/
				statement.setString(4, cla.getRoom_number());
				
				statement.executeUpdate();
				
				System.out.println("Anadido con exito");
				
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		

	@Override
	public void modificar(Classroom cla, String identificador) {
		// TODO Auto-generated method stub
try {
			
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE classroom SET building = ?, room_number = ?,capacity, WHERE building = ?");
			statement.setString(1, cla.getBuilding());
			statement.setString(2, cla.getRoom_number());
			
			/*String departmentId = co.getDepartment().getDept_name();
            
           PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
            statement2.setString(1, departmentId);
            
           ResultSet deptRS = statement2.executeQuery();
            
            Department d = new Department();
            d.setDept_name(departmentId);
            
            while(deptRS.next()) {
            	d.setBuilding(deptRS.getString("building"));
            	d.setDept_name(deptRS.getString("dept_name"));
            	d.setBudget(deptRS.getInt("budget"));
            }

			statement.setString(3, d.getDept_name());*/
			
			statement.setString(4, cla.getCapacity());
			statement.setString(5, identificador);
			
			statement.executeUpdate();
			
			System.out.println("Modificado con exito " + cla.getBuilding());
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void recuperar(Classroom cla) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE classroom SET isdelete = false WHERE building =?");
			statement.setString(1, cla.getBuilding());
			
			statement.executeUpdate();
			
			System.out.println( cla.getBuilding());
			System.out.println("Recuperado con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Classroom> buscarTodos() {
		// TODO Auto-generated method stub
List<Classroom> classroom = new ArrayList<>();
		
		try {
			PreparedStatement statement = con.getCon().prepareStatement("SELECT * FROM classroom where isdelete = false");
			
			ResultSet rs = statement.executeQuery();
			 
            while(rs.next()) {
                Classroom cla = new Classroom();
                cla.setBuilding(rs.getString("building"));
                System.out.println(rs.getString("building"));
                cla.setRoom_number(rs.getString("numero de salon"));
              //  String departmentId = rs.getString("dept_name");
                
              /*  PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
                statement2.setString(1, departmentId);
                
                ResultSet deptRS = statement2.executeQuery();
                
                Department d = new Department();
                d.setDept_name(departmentId);
                
                while(deptRS.next()) {
                	d.setBuilding(deptRS.getString("building"));
                	d.setDept_name(deptRS.getString("dept_name"));
                	d.setBudget(deptRS.getInt("budget"));
                }
                
                co.setDepartment(d);
                co.setCredits(rs.getInt("credits"));
                courses.add(co);
            }*/
            
            System.out.println(classroom);
            return classroom;
                
		} 
		}
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return classroom;
	
	}

	@Override
	public List<Classroom> buscarEliminados() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				List<Classroom> classroom = new ArrayList<>();
				
				try {
					PreparedStatement statement = con.getCon().prepareStatement("SELECT * FROM classroom where isdelete = true");
					
					ResultSet rs = statement.executeQuery();
					 
		            while(rs.next()) {
		                Classroom cla = new Classroom();
		                cla.setBuilding(rs.getString("building"));
		                cla.setRoom_number(rs.getString("room_number"));
		                
		              /*  String departmentId = rs.getString("dept_name");
		                
		                PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
		                statement2.setString(1, departmentId);
		                
		                ResultSet deptRS = statement2.executeQuery();
		                
		                Department d = new Department();
		                d.setDept_name(departmentId);
		                
		                while(deptRS.next()) {
		                	d.setBuilding(deptRS.getString("building"));
		                	d.setDept_name(deptRS.getString("dept_name"));
		                	d.setBudget(deptRS.getInt("budget"));
		                }
		                
		                co.setDepartment(d);
		                co.setCredits(rs.getInt("credits"));
		                courses.add(co);
		            }*/
		            
		            System.out.println(classroom);
		            return classroom;
		                
				} 
				}
				catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
				return classroom;
		
	}

	@Override
	public void eliminar(Classroom cla) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE classroom SET isdelete = true WHERE building = ?");
			statement.setString(1, cla.getBuilding());
			
			statement.executeUpdate();
			
			System.out.println("Eliminado con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public static void main(String args[]) {
		ClassRoomDAO dao= new ClassRoomDAO();
		//Department d = new Department();
		//d.setDept_name("Probando2");
		//d.setBuilding("DePrueba");
		//d.setBudget(1234);
		Classroom cla = new Classroom ("packard", "5", null, null, false);	
		
		dao.eliminar(cla);
		dao.buscarTodos();
		dao.buscarEliminados();
	}




}
