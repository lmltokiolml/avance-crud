package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.database.MysqlConnect;
import model.entities.Department;

public class DepartmentDAO implements IDepartmentDAO {
	
	MysqlConnect con;
	
	public DepartmentDAO(){
		con = new MysqlConnect();
		con.connect();
	}

	@Override
	public void agregar(Department d) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("INSERT INTO department"
																		+ "(dept_name, building, budget)" 
																		+ "VALUES(?,?,?)");
			statement.setString(1, d.getDept_name());
			statement.setString(2, d.getBuilding());
			statement.setInt(3, d.getBudget());
			
			statement.executeUpdate();
			
			System.out.println("A�adido con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void modificar(Department d, String identificador) {
		// TODO Auto-generated method stuba
		try {
			
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE department SET dept_name = ?, building = ?, budget = ? WHERE dept_name = ?");
			statement.setString(1, d.getDept_name());
			statement.setString(2, d.getBuilding());
			statement.setInt(3, d.getBudget());
			statement.setString(4, identificador);
			
			statement.executeUpdate();
			
			System.out.println("Modificado con exito " + d.getDept_name());
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void eliminar(Department d) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE department SET isdelete = true WHERE dept_name=?");
			statement.setString(1, d.getDept_name());
			
			statement.executeUpdate();
			
			System.out.println("Eliminado con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void recuperar(Department d) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE department SET isdelete = false WHERE dept_name=?");
			statement.setString(1, d.getDept_name());
			
			statement.executeUpdate();
			
			System.out.println("Eliminado con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Department> buscarTodos() {
		// TODO Auto-generated method stub
		List<Department> departments = new ArrayList<>();
		
		try {
			PreparedStatement statement = con.getCon().prepareStatement("SELECT * FROM department where isdelete = false");
			
			ResultSet rs = statement.executeQuery();
			 
            while(rs.next()) {
                Department d = new Department();
                d.setDept_name(rs.getString("dept_name"));
                d.setBuilding(rs.getString("building"));
                d.setBudget(rs.getInt("budget"));
                departments.add(d);
            }
            
            System.out.println(departments);
            return departments;
                
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Department> buscarEliminados() {
		// TODO Auto-generated method stub
		List<Department> departments = new ArrayList<>();
		
		try {
			PreparedStatement statement = con.getCon().prepareStatement("SELECT * FROM department where isdelete = true");
			
			ResultSet rs = statement.executeQuery();
			 
            while(rs.next()) {
                Department d = new Department();
                d.setDept_name(rs.getString("dept_name"));
                d.setBuilding(rs.getString("building"));
                d.setBudget(rs.getInt("budget"));
                departments.add(d);
            }
            
            System.out.println(departments);
            return departments;
                
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String args[]) {
		DepartmentDAO dao= new DepartmentDAO();		
		
		Department d = new Department("asda","asda",1231, null, null, null, false);
		
		dao.buscarTodos();
		dao.buscarEliminados();
	}

}
