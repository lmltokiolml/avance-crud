package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.omg.Messaging.SyncScopeHelper;

import model.database.MysqlConnect;
import model.entities.Course;
import model.entities.Department;

public class CourseDAO implements ICourseDAO{
	
	MysqlConnect con;
	
	public CourseDAO(){
		con = new MysqlConnect();
		
		con.connect();
	}

	@Override
	public void agregar(Course co) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("INSERT INTO course"
																		+ "(course_id, title, dept_name, credits)" 
																		+ "VALUES(?,?,?,?)");
			statement.setString(1, co.getCourse_id());
			statement.setString(2, co.getTitle());
			
			String departmentId = co.getDepartment().getDept_name();
            
            PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
            statement2.setString(1, departmentId);
            
            ResultSet deptRS = statement2.executeQuery();
            
            Department d = new Department();
            d.setDept_name(departmentId);
            
            while(deptRS.next()) {
            	d.setBuilding(deptRS.getString("building"));
            	d.setDept_name(deptRS.getString("dept_name"));
            	d.setBudget(deptRS.getInt("budget"));
            }

			statement.setString(3, d.getDept_name());
			statement.setInt(4, co.getCredits());
			
			statement.executeUpdate();
			
			System.out.println("Anadido con exito");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void modificar(Course co, String identificador) {
		// TODO Auto-generated method stub
		try {
			
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE course SET course_id = ?, title = ?, dept_name = ?, credits = ? WHERE course_id = ?");
			statement.setString(1, co.getCourse_id());
			statement.setString(2, co.getTitle());
			
			String departmentId = co.getDepartment().getDept_name();
            
           PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
            statement2.setString(1, departmentId);
            
           ResultSet deptRS = statement2.executeQuery();
            
            Department d = new Department();
            d.setDept_name(departmentId);
            
            while(deptRS.next()) {
            	d.setBuilding(deptRS.getString("building"));
            	d.setDept_name(deptRS.getString("dept_name"));
            	d.setBudget(deptRS.getInt("budget"));
            }

			statement.setString(3, d.getDept_name());
			
			statement.setInt(4, co.getCredits());
			statement.setString(5, identificador);
			
			statement.executeUpdate();
			
			System.out.println("Modificado con exito " + co.getCourse_id());
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void eliminar(Course co) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE course SET isdelete = true WHERE course_id = ?");
			statement.setString(1, co.getCourse_id());
			
			statement.executeUpdate();
			
			System.out.println("Eliminado con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Course> buscarTodos() {
		// TODO Auto-generated method stub
		List<Course> courses = new ArrayList<>();
		
		try {
			PreparedStatement statement = con.getCon().prepareStatement("SELECT * FROM course where isdelete = false");
			
			ResultSet rs = statement.executeQuery();
			 
            while(rs.next()) {
                Course co = new Course();
                co.setCourse_id(rs.getString("course_id"));
                System.out.println(rs.getString("course_id"));
                co.setTitle(rs.getString("title"));
                String departmentId = rs.getString("dept_name");
                
                PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
                statement2.setString(1, departmentId);
                
                ResultSet deptRS = statement2.executeQuery();
                
                Department d = new Department();
                d.setDept_name(departmentId);
                
                while(deptRS.next()) {
                	d.setBuilding(deptRS.getString("building"));
                	d.setDept_name(deptRS.getString("dept_name"));
                	d.setBudget(deptRS.getInt("budget"));
                }
                
                co.setDepartment(d);
                co.setCredits(rs.getInt("credits"));
                courses.add(co);
            }
            
            System.out.println(courses);
            return courses;
                
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void recuperar(Course co) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("UPDATE course SET isdelete = false WHERE course_id =?");
			statement.setString(1, co.getCourse_id());
			
			statement.executeUpdate();
			
			System.out.println( co.getCourse_id());
			System.out.println("Recuperado con exito ");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Course> buscarEliminados() {
		// TODO Auto-generated method stub
		List<Course> courses = new ArrayList<>();
		
		try {
			PreparedStatement statement = con.getCon().prepareStatement("SELECT * FROM course where isdelete = true");
			
			ResultSet rs = statement.executeQuery();
			 
            while(rs.next()) {
                Course co = new Course();
                co.setCourse_id(rs.getString("course_id"));
                co.setTitle(rs.getString("title"));
                
                String departmentId = rs.getString("dept_name");
                
                PreparedStatement statement2 = con.getCon().prepareStatement("select * from department where dept_name = ?");
                statement2.setString(1, departmentId);
                
                ResultSet deptRS = statement2.executeQuery();
                
                Department d = new Department();
                d.setDept_name(departmentId);
                
                while(deptRS.next()) {
                	d.setBuilding(deptRS.getString("building"));
                	d.setDept_name(deptRS.getString("dept_name"));
                	d.setBudget(deptRS.getInt("budget"));
                }
                
                co.setDepartment(d);
                co.setCredits(rs.getInt("credits"));
                courses.add(co);
            }
            
            System.out.println(courses);
            return courses;
                
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String args[]) {
		CourseDAO dao= new CourseDAO();
		Department d = new Department();
		d.setDept_name("Probando2");
		d.setBuilding("DePrueba");
		d.setBudget(1234);
		Course co = new Course ("BIO-501", "asgasg", d , 5, null, false);	
		
		dao.eliminar(co);
		dao.buscarTodos();
		dao.buscarEliminados();
	}



}
