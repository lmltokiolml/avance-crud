package model.entities;

import java.util.List;

public class Course {
	
	private String course_id;
	private String title;
	private Department department;
	private int credits;
	private List<Section> section;
	private boolean isDeleted;
	
	public  Course()
	{
		this.isDeleted= false;
	}
	
	
	public Course(String couser_id, String title, Department department, int credits, List<Section> section,
			boolean isDeleted) {
		
		this.course_id = couser_id;
		this.title = title;
		this.department = department;
		this.credits = credits;
		this.section = section;
		this.isDeleted = isDeleted;
	}


	public String getCourse_id() {
		return course_id;
	}
	public void setCourse_id(String course_id) {
		this.course_id = course_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	public List<Section> getSection() {
		return section;
	}
	public void setSection(List<Section> section) {
		this.section = section;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	

}
