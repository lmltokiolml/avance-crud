package model.entities;

public class Time_slot {
	
	private String time_slot_id;
	private String day;
	private int start_hr;
	private int  start_min;
	private int end_hr;
	private int end_min;
	
	private boolean isDeleted;
	
	
	public  Time_slot()
	{
		this.isDeleted= false;
	}
	
	
	public Time_slot(String time_slot_id, String day, int start_hr, int start_min, int end_hr, int end_min,
			boolean isDeleted) {
		
		this.time_slot_id = time_slot_id;
		this.day = day;
		this.start_hr = start_hr;
		this.start_min = start_min;
		this.end_hr = end_hr;
		this.end_min = end_min;
		this.isDeleted = isDeleted;
	}


	public String getTime_slot_id() {
		return time_slot_id;
	}
	public void setTime_slot_id(String time_slot_id) {
		this.time_slot_id = time_slot_id;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public int getStart_hr() {
		return start_hr;
	}
	public void setStart_hr(int start_hr) {
		this.start_hr = start_hr;
	}
	public int getStart_min() {
		return start_min;
	}
	public void setStart_min(int start_min) {
		this.start_min = start_min;
	}
	public int getEnd_hr() {
		return end_hr;
	}
	public void setEnd_hr(int end_hr) {
		this.end_hr = end_hr;
	}
	public int getEnd_min() {
		return end_min;
	}
	public void setEnd_min(int end_min) {
		this.end_min = end_min;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

}
