package model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnect {
	
	Connection con = null;
	
	public void connect() {
		try {
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/university?" 
							+ "useUnicode=true&"
							+ "useJDBCCompilantTimezoneShift=true&"
							+ "useLegacyDatetimeCode=false&"
							+ "serverTimezone=UTC","root","");
			System.out.println("Coneccion satisfactoria");

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());

		}
	}
	
	public Connection getCon() {
		return con;
	}
	
	public void setCon(Connection con) {
		this.con = con;
	}
	
	public static void main(String args[]) {
		MysqlConnect con = new MysqlConnect();
		con.connect();
	}

}
