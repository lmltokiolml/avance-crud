package model.tableModel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.DepartmentDAO;
import model.entities.Department;

public class DepartmentTableModel extends AbstractTableModel{
	
	DepartmentDAO dao = new DepartmentDAO();
	List<Department> departments = new ArrayList<Department>();
	List<Department> departmentsAux = new ArrayList<Department>();
	
	public DepartmentTableModel() {
		// TODO Auto-generated constructor stub
		cargarTodos();
	}
	
	public void cargarTodos() {
		
		departments = dao.buscarTodos();
		
		departmentsAux = new ArrayList<Department>(departments);
	}
	
	public void buscar(String palabraDeBusqueda) {
		departmentsAux = new ArrayList<Department>(departments);
		
		for(Department d: departments) {
			if(!d.getBuilding().toLowerCase().contains(palabraDeBusqueda.toLowerCase()) &&
				!d.getDept_name().toLowerCase().contains(palabraDeBusqueda.toLowerCase())) {
				departmentsAux.remove(d);
			}
		}
	}
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return departmentsAux.size();
	}
	
	public String getColumnName(int column) {
		 if(column==0) {
			 return "Nombre";
		 }
		 else if(column==1) {
			 return "Edificio";
		 }
		 else {
			 return "Presupuesto";
		 }
		
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if(columna==0) {
			 return departmentsAux.get(fila).getDept_name();
		 }
		 else if(columna==1) {
			 return departmentsAux.get(fila).getBuilding();
		 }
		 else {
			 return departmentsAux.get(fila).getBudget();
		 }
	}

}
