package model.tableModel;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.ClassRoomDAO;
import model.entities.Classroom;
public class ClassRoomTableModel extends AbstractTableModel  {
	
	ClassRoomDAO dao = new ClassRoomDAO();
	List<Classroom> ClassRoom = new ArrayList<Classroom>();
	List<Classroom> classRoomAux = new ArrayList<Classroom>();
	
	public ClassRoomTableModel() {
		// TODO Auto-generated constructor stub
		cargarTodos();
	}
	
	

	public void cargarTodos() {
		// TODO Auto-generated method stub
		ClassRoom = dao.buscarTodos();
		
		ClassRoom = new ArrayList<Classroom>(ClassRoom);
		
	}

	public void buscar(String palabraDeBusqueda) {
		ClassRoom = new ArrayList<Classroom>(ClassRoom);
		
		for(Classroom c: ClassRoom) {
			if(!c.getBuilding().toLowerCase().contains(palabraDeBusqueda.toLowerCase()) &&
				!c.getRoom_number().toLowerCase().contains(palabraDeBusqueda.toLowerCase()) &&
				!c.getCapacity().toLowerCase().contains(palabraDeBusqueda.toLowerCase())) {
				ClassRoom.remove(c);
			}
		}
	}



	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return ClassRoom.size();
	}
	
	public String getColumnName(int column) {
		 if(column==0) {
			 return "salon";
		 }
		 else if(column==1) {
			 return "numero de salon";
		 }
		 else if(column==2) {
			 return "capacidad";
		 }
		return null;
		
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if(columna==0) {
			 return ClassRoom.get(fila).getBuilding();
		 }
		 else if(columna==1) {
			 return ClassRoom.get(fila).getRoom_number();
		 }
		 else if(columna==2) {
			 return ClassRoom.get(fila).getCapacity();
		 }
		 else {
			 return ClassRoom.get(fila).getClass();
		 }
	}


}
