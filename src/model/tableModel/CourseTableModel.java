package model.tableModel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.CourseDAO;
import model.entities.Course;

public class CourseTableModel extends AbstractTableModel {
	
	CourseDAO dao = new CourseDAO();
	List<Course> courses = new ArrayList<Course>();
	List<Course> coursesAux = new ArrayList<Course>();
	
	public CourseTableModel() {
		// TODO Auto-generated constructor stub
		cargarTodos();
	}
	
	public void cargarTodos() {
		
		courses = dao.buscarTodos();
		
		coursesAux = new ArrayList<Course>(courses);
	}
	
	public void buscar(String palabraDeBusqueda) {
		coursesAux = new ArrayList<Course>(courses);
		
		for(Course c: courses) {
			if(!c.getCourse_id().toLowerCase().contains(palabraDeBusqueda.toLowerCase()) &&
				!c.getTitle().toLowerCase().contains(palabraDeBusqueda.toLowerCase()) &&
				!c.getDepartment().getDept_name().toLowerCase().contains(palabraDeBusqueda.toLowerCase())) {
				coursesAux.remove(c);
			}
		}
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return coursesAux.size();
	}
	
	public String getColumnName(int column) {
		 if(column==0) {
			 return "ID";
		 }
		 else if(column==1) {
			 return "Nombre";
		 }
		 else if(column==2) {
			 return "Departamento";
		 }
		 else {
			 return "Creditos";
		 }
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if(columna==0) {
			 return coursesAux.get(fila).getCourse_id();
		 }
		 else if(columna==1) {
			 return coursesAux.get(fila).getTitle();
		 }
		 else if(columna==2) {
			 return coursesAux.get(fila).getDepartment().getDept_name();
		 }
		 else {
			 return coursesAux.get(fila).getCredits();
		 }
	}

}
