package model.tableModel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.DepartmentDAO;
import model.entities.Department;

public class DepartmentDeleteTableModel extends AbstractTableModel {
	
	DepartmentDAO dao = new DepartmentDAO();
	List<Department> departments = new ArrayList<Department>();
	List<Department> departmentsDel = new ArrayList<Department>();
	
	public DepartmentDeleteTableModel() {
		// TODO Auto-generated constructor stub
		cargarEliminados();
	}
	
	public void cargarEliminados() {
		
		departments = dao.buscarEliminados();
		
		departmentsDel = new ArrayList<Department>(departments);
	}
	
	public void buscar(String palabraDeBusqueda) {
		departmentsDel = new ArrayList<Department>(departments);
		
		for(Department d: departments) {
			if(!d.getBuilding().toLowerCase().contains(palabraDeBusqueda.toLowerCase()) &&
				!d.getDept_name().toLowerCase().contains(palabraDeBusqueda.toLowerCase())) {
				departmentsDel.remove(d);
			}
		}
	}
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return departmentsDel.size();
	}
	
	public String getColumnName(int column) {
		 if(column==0) {
			 return "Nombre";
		 }
		 else if(column==1) {
			 return "Edificio";
		 }
		 else {
			 return "Presupuesto";
		 }
		
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if(columna==0) {
			 return departmentsDel.get(fila).getDept_name();
		 }
		 else if(columna==1) {
			 return departmentsDel.get(fila).getBuilding();
		 }
		 else {
			 return departmentsDel.get(fila).getBudget();
		 }
	}

}
