package model.tableModel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.CourseDAO;
import model.entities.Course;

public class CourseDeleteTableModel extends AbstractTableModel {

	CourseDAO dao = new CourseDAO();
	List<Course> courses = new ArrayList<Course>();
	List<Course> coursesDel = new ArrayList<Course>();
	
	public CourseDeleteTableModel() {
		// TODO Auto-generated constructor stub
		cargarEliminados();
	}
	
	public void cargarEliminados() {
		
		courses = dao.buscarEliminados();
		
		coursesDel = new ArrayList<Course>(courses);
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return coursesDel.size();
	}
	
	public String getColumnName(int column) {
		 if(column==0) {
			 return "ID";
		 }
		 else if(column==1) {
			 return "Nombre";
		 }
		 else if(column==2) {
			 return "Departamento";
		 }
		 else {
			 return "Creditos";
		 }
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if(columna==0) {
			 return coursesDel.get(fila).getCourse_id();
		 }
		 else if(columna==1) {
			 return coursesDel.get(fila).getTitle();
		 }
		 else if(columna==2) {
			 return coursesDel.get(fila).getDepartment().getDept_name();
		 }
		 else {
			 return coursesDel.get(fila).getCredits();
		 }
	}
}
