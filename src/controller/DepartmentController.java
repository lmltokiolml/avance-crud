package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.dao.DepartmentDAO;
import model.entities.Department;
import model.tableModel.DepartmentDeleteTableModel;
import model.tableModel.DepartmentTableModel;
import view.DepartmentDelete;
import view.DepartmentView;

public class DepartmentController {
	
	DepartmentTableModel tableModel;
	DepartmentView vista;
	DepartmentDelete vistaEliminados = new DepartmentDelete();
	DepartmentDAO dao = new DepartmentDAO();
	Department d = new Department();
	JTable tabla;
	JTable tablaE;
	
	public DepartmentController(DepartmentTableModel tableModel, DepartmentView vista) {
		super();
		this.tableModel = tableModel;
		this.vista = vista;
		tabla = new JTable(tableModel);
		vista.getScrollPane().setViewportView(tabla);
		
		vista.getBtnEliminar().addActionListener(e->eliminarDepartment());
		
		vista.getBtnGuardar().addActionListener(e->guardarDepartmento());
		
		vista.getBtnModificar().addActionListener(e->modificarDatos());
		
		vista.getBtnEliminados().addActionListener(e->verEliminados());
		
		vistaEliminados.getBtnRecuperar().addActionListener(e->recuperarDepartment());
		
		vistaEliminados.getBtnVolver().addActionListener(e->volver());
		
		vista.getTxtBuscador().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println(DepartmentController.this.vista.getTxtBuscador().getText());
				DepartmentController.this.buscarDepartment(DepartmentController.this.vista.getTxtBuscador().getText());
			}
		});
		
		verDatos();
		
	}
	
	private void guardarDepartmento() {
		d.setBuilding(vista.getTxtEdificio().getText());
		d.setDept_name(vista.getTxtDeptName().getText());
		d.setBudget(Integer.parseInt(vista.getTxtPresupuesto().getText()));
		
		dao.agregar(d);
		
		recargarTabla();
		
		limpiarDatos();
	
	}
	
	private void verDatos() {
		
		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	vista.getTxtDeptName().setText(tabla.getValueAt(tabla.getSelectedRow(),0).toString());
	        	vista.getTxtEdificio().setText(tabla.getValueAt(tabla.getSelectedRow(),1).toString());
	        	vista.getTxtPresupuesto().setText(tabla.getValueAt(tabla.getSelectedRow(),2).toString());
	        	d.setDept_name(vista.getTxtDeptName().getText());
	        }
	        
	    });
	}
	
	private void modificarDatos() {
		
		String identificador = d.getDept_name();
		
		d.setBuilding(vista.getTxtEdificio().getText());
		d.setDept_name(vista.getTxtDeptName().getText());
		d.setBudget(Integer.parseInt(vista.getTxtPresupuesto().getText()));
		
		System.out.println(d.getDept_name());
		
		dao.modificar(d,identificador);
		
		recargarTabla();
		
		limpiarDatos();
		
	}
	
	private void eliminarDepartment() {
		
		System.out.println(d.getDept_name());
		dao.eliminar(d);
		
		recargarTabla();
		
		limpiarDatos();
		
	}
	
	private void buscarDepartment(String palabraDeBusqueda) {
		
		tableModel.buscar(palabraDeBusqueda);;
		tabla.setModel(tableModel);
		vista.getScrollPane().setViewportView(tabla);
	}
	
	private void recargarTabla() {
		
		tableModel.cargarTodos();
		tabla.setModel(tableModel);
		vista.getScrollPane().setViewportView(tabla);
		
	}
	
	private void limpiarDatos() {
		d.setBuilding(null);
		d.setDept_name(null);
		d.setBudget(0);
		
		vista.getTxtEdificio().setText("");
		vista.getTxtDeptName().setText("");
		vista.getTxtPresupuesto().setText("");
		vista.getTxtBuscador().setText("");
		
		vistaEliminados.getTxtEdificio().setText("");
		vistaEliminados.getTxtDeptName().setText("");
		vistaEliminados.getTxtPresupuesto().setText("");
	
	}
	
	private void verEliminados() {
		
		recargarTablaE();
		 
		verDatosEliminados();
		vistaEliminados.setVisible(true);
		vista.setVisible(false);
		
	}
	
	private void recargarTablaE() {
		DepartmentDeleteTableModel tableModel1 = new DepartmentDeleteTableModel();
		
		tablaE = new JTable(tableModel1);
		
		tableModel1.cargarEliminados();
		tablaE.setModel(tableModel1);
		vistaEliminados.getScrollPane().setViewportView(tablaE);
	}
	
	private void verDatosEliminados() {
		
		tablaE.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	vistaEliminados.getTxtDeptName().setText(tablaE.getValueAt(tablaE.getSelectedRow(),0).toString());
	        	vistaEliminados.getTxtEdificio().setText(tablaE.getValueAt(tablaE.getSelectedRow(),1).toString());
	        	vistaEliminados.getTxtPresupuesto().setText(tablaE.getValueAt(tablaE.getSelectedRow(),2).toString());
	        	d.setDept_name(vistaEliminados.getTxtDeptName().getText());
	        }
	        
	    });
	
	}
	
	private void volver() {
		// TODO Auto-generated method stub
		vistaEliminados.setVisible(false);
		vista.setVisible(true);
		
		recargarTabla();
		
		limpiarDatos();
	}


	private void recuperarDepartment() {
		// TODO Auto-generated method stub
		dao.recuperar(d);
		
		limpiarDatos();
		
		verEliminados();
	}
	

}
