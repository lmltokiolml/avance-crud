package controller;

import model.tableModel.ClassRoomTableModel;
import model.tableModel.CourseTableModel;
import model.tableModel.DepartmentTableModel;
import view.ClassRoomView;
import view.CourseView;
import view.DepartmentView;
import view.University;

public class UniversityController {
	
	University vista;
	
	public UniversityController(University vista) {
		super();
		this.vista = vista;
		
		
		vista.getBtnCursos().addActionListener(e->irCursos());
		
		vista.getBtnDepartamentos().addActionListener(e->irDepartamentos());
		vista.getBtnClassroom().addActionListener(e-> irclassroom());
	}

	private void irclassroom() {
		// TODO Auto-generated method stub
		
			
			ClassRoomTableModel model = new ClassRoomTableModel();
			ClassRoomView view= new ClassRoomView();
			ClassroomController controller = new ClassroomController (model,view);
			
			view.setVisible(true);	
	}

	private void irDepartamentos() {
		// TODO Auto-generated method stub
		DepartmentTableModel model = new DepartmentTableModel();
		DepartmentView view = new DepartmentView();
		DepartmentController controller = new DepartmentController(model, view);
	
		view.setVisible(true);
		
	}

	private void irCursos() {
		// TODO Auto-generated method stub
		
		CourseTableModel model = new CourseTableModel();
		CourseView view = new CourseView();
		CourseController controller = new CourseController(model, view);
	
		view.setVisible(true);
	}
	
	

}