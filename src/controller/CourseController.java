package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.dao.CourseDAO;
import model.entities.Course;
import model.entities.Department;
import model.tableModel.CourseDeleteTableModel;
import model.tableModel.CourseTableModel;
import model.tableModel.DepartmentTableModel;
import view.CourseDelete;
import view.CourseView;

public class CourseController {
	CourseTableModel tableModel;
	CourseView vista;
	CourseDelete vistaEliminados = new CourseDelete();
	CourseDAO dao = new CourseDAO();
	Course c = new Course();
	JTable tabla;
	JTable tablaE;
	
	
	public CourseController(CourseTableModel tableModel, CourseView vista) {
		super();
		
		
		this.tableModel = tableModel;
		this.vista = vista;
		tabla = new JTable(tableModel);
		
		cargarCombobox();
		
		vista.getScrollPane().setViewportView(tabla);
		vista.getBtnEliminar().addActionListener(e->eliminarCourse());
		
		vista.getBtnGuardar().addActionListener(e->guardarCourse());
		
		vista.getBtnModificar().addActionListener(e->modificarDatos());
		
		vista.getBtnEliminados().addActionListener(e->verEliminados());
		
		vistaEliminados.getBtnRecuperar().addActionListener(e->recuperarCourse());
		
		vistaEliminados.getBtnVolver().addActionListener(e->volver());
		
		vista.getTxtBuscador().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println(CourseController.this.vista.getTxtBuscador().getText());
				CourseController.this.buscarCourse(CourseController.this.vista.getTxtBuscador().getText());
			}
		});
		
		verDatos();
		
	}
	
	private void guardarCourse() {
		c.setCourse_id(vista.getTxtCursoID().getText());
		c.setTitle(vista.getTxtTitle().getText());
		Department d = new Department();
		
		String valor = vista.getComboBox().getSelectedItem().toString();
		d.setDept_name(valor);
		
		c.setDepartment(d);
		c.setCredits(Integer.parseInt(vista.getTxtCreditos().getText()));
		
		dao.agregar(c);
		
		recargarTabla();
		
		limpiarDatos();
	
	}
	
	private void verDatos() {
		
		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	vista.getTxtCursoID().setText(tabla.getValueAt(tabla.getSelectedRow(),0).toString());
	        	vista.getTxtTitle().setText(tabla.getValueAt(tabla.getSelectedRow(),1).toString());
	        	vista.getComboBox().setSelectedItem(tabla.getValueAt(tabla.getSelectedRow(),2).toString());
	        	vista.getTxtCreditos().setText(tabla.getValueAt(tabla.getSelectedRow(),3).toString());
	        	c.setCourse_id(vista.getTxtCursoID().getText());
	        }
	        
	    });
	}
	
	private void modificarDatos() {
		
		String identificador = c.getCourse_id();
		Department d= new Department();
		
		c.setCourse_id(vista.getTxtCursoID().getText());
		c.setTitle(vista.getTxtTitle().getText());
		
		c.setCredits(Integer.parseInt(vista.getTxtCreditos().getText()));
		String valor= vista.getComboBox().getSelectedItem().toString();
		d.setDept_name(valor);
		c.setDepartment(d);
		System.out.println(c.getCourse_id());

		dao.modificar(c, identificador);
		
		recargarTabla();
		
		limpiarDatos();
		
	}
	
	private void eliminarCourse() {
		
		System.out.println(c.getCourse_id());
		dao.eliminar(c);
		
		recargarTabla();
		
		limpiarDatos();
		
	}
	
	private void buscarCourse(String palabraDeBusqueda) {
		
		tableModel.buscar(palabraDeBusqueda);;
		tabla.setModel(tableModel);
		vista.getScrollPane().setViewportView(tabla);
	}
	
	private void recargarTabla() {
		
		tableModel.cargarTodos();
		tabla.setModel(tableModel);
		vista.getScrollPane().setViewportView(tabla);
		
	}
	
	private void limpiarDatos() {
		c.setCourse_id(null);
		c.setTitle(null);
		c.setCredits(0);
		
		vista.getTxtCursoID().setText("");
		vista.getTxtTitle().setText("");
		vista.getTxtCreditos().setText("");
	
	}
	
	private void cargarCombobox() {
		DefaultComboBoxModel mdlCombo = (DefaultComboBoxModel) vista.getComboBox().getModel() ;
		DepartmentTableModel tableModel2 = new DepartmentTableModel();
		mdlCombo.removeAllElements();
		
        for (int i = 0; i < tableModel2.getRowCount(); i++) {   	
            	System.out.println(tableModel2.getValueAt(i, 0)+"tabla agregado");
                mdlCombo.addElement(tableModel2.getValueAt(i, 0));
            	System.out.println(mdlCombo.getElementAt(i)+"combo agregado");
        }

	}
	
	private void verEliminados() {
		
		recargarTablaE();
		 
		verDatosEliminados();
		vistaEliminados.setVisible(true);
		vista.setVisible(false);
		
	}
	
	private void recargarTablaE() {
		CourseDeleteTableModel tableModel1 = new CourseDeleteTableModel();
		
		tablaE = new JTable(tableModel1);
		
		tableModel1.cargarEliminados();
		tablaE.setModel(tableModel1);
		vistaEliminados.getScrollPane().setViewportView(tablaE);
	}
	
	private void verDatosEliminados() {
		
		
	
	}
	
	private void volver() {
		// TODO Auto-generated method stub
		vistaEliminados.setVisible(false);
		vista.setVisible(true);
		
		recargarTabla();
		
		limpiarDatos();
	}


	private void recuperarCourse() {
		// TODO Auto-generated method stub
		tablaE.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	
	        }
	        
	    });
		
		c.setCourse_id(tablaE.getValueAt(tablaE.getSelectedRow(),0).toString());
		System.out.println(tablaE.getValueAt(tablaE.getSelectedRow(),0).toString());
		System.out.println(c.getCourse_id());
		
		dao.recuperar(c);
		
		limpiarDatos();
		
		verEliminados();
	}

}
