package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.dao.ClassRoomDAO;
import model.entities.Classroom;
import model.entities.Department;
import model.tableModel.ClassRoomTableModel;
import view.ClassRoomDelete;
import view.ClassRoomView;

public class ClassroomController {
	ClassRoomTableModel tableModel;
	ClassRoomView vista;
	ClassRoomDelete vistaEliminados = new ClassRoomDelete();
	ClassRoomDAO dao = new ClassRoomDAO();
	Classroom cla = new Classroom();
	JTable tabla;
	JTable tablaE;

public ClassroomController(ClassRoomTableModel tableModel, ClassRoomView vista) {
	super();
	
	
	this.tableModel = tableModel;
	this.vista = vista;
	tabla = new JTable(tableModel);
	cargarCombobox();
	
	vista.getScrollPane().setViewportView(tabla);
	
	vista.getBtnEliminar().addActionListener(e->eliminarClassroom());
	
	vista.getBtnGuardar().addActionListener(e->eliminarClassroom());
	
	vista.getBtnModificar().addActionListener(e->modificarDatos());
	
	vista.getBtnEliminados().addActionListener(e->verEliminados());
	
	vistaEliminados.getBtnRecuperar().addActionListener(e->recuperarClassroom());
	
	vistaEliminados.getBtnVolver().addActionListener(e->volver());
	
	vista.getTxtBuscador().addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent e) {
			System.out.println(ClassroomController.this.vista.getTxtBuscador().getText());
			ClassroomController.this.buscarClassroom(ClassroomController.this.vista.getTxtBuscador().getText());
		}
	});
	
	verDatos();
	
}
private void cargarCombobox() {
	// TODO Auto-generated method stub
	
}
private void verDatos() {
	// TODO Auto-generated method stub
	tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
        	vista.getTxtbulding().setText(tabla.getValueAt(tabla.getSelectedRow(),0).toString());
        	vista.getComboBox().setSelectedItem(tabla.getValueAt(tabla.getSelectedRow(),2).toString());
        	vista.getTxtCapacity().setText(tabla.getValueAt(tabla.getSelectedRow(),3).toString());
        	cla.setBuilding(vista.getTxtbulding().getText());
        }
        
    });
}

protected void buscarClassroom(String palabraDeBusqueda) {
	// TODO Auto-generated method stub
	tableModel.buscar(palabraDeBusqueda);;
	tabla.setModel(tableModel);
	vista.getScrollPane().setViewportView(tabla);
}

private Object volver() {
	// TODO Auto-generated method stub
	vistaEliminados.setVisible(false);
	vista.setVisible(true);
	
	recargarTabla();
	
	limpiarDatos();
	return null;
}

private void limpiarDatos() {
	// TODO Auto-generated method stub
	cla.setBuilding(null);
	cla.setRoom_number(null);
	cla.setCapacity(null);
	
	vista.getTxtbulding().setText("");
	vista.getTxtCapacity().setText("");

	
}
private void recargarTabla() {
	// TODO Auto-generated method stub
	tableModel.cargarTodos();
	tabla.setModel(tableModel);
	vista.getScrollPane().setViewportView(tabla);
	
}
private void recuperarClassroom() {
	// TODO Auto-generated method stub
	tablaE.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
        	
        }
        
    });
	
	cla.setBuilding (tablaE.getValueAt(tablaE.getSelectedRow(),0).toString());
	System.out.println(tablaE.getValueAt(tablaE.getSelectedRow(),0).toString());
	System.out.println(cla.getBuilding());
	
	dao.recuperar(cla);
	
	limpiarDatos();
	
	verEliminados();
}

private void verEliminados() {
	// TODO Auto-generated method stub
	
	
}
private void modificarDatos() {
	// TODO Auto-generated method stub
	String identificador = cla.getBuilding();
	Department d= new Department();
	
	cla.setBuilding(vista.getTxtbulding().getText());
	cla.setCapacity(vista.getTxtCapacity().getText());
	
	/*c.setCredits(Integer.parseInt(vista.getTxtCreditos().getText()));
	String valor= vista.getComboBox().getSelectedItem().toString();
	d.setDept_name(valor);
	c.setDepartment(d);*/
	System.out.println(cla.getBuilding());

	dao.modificar(cla, identificador);
	
	recargarTabla();
	
	limpiarDatos();
	
}
private void eliminarClassroom() {
	// TODO Auto-generated method stub
	System.out.println(cla.getBuilding());
	dao.eliminar(cla);
	
	recargarTabla();
	
	limpiarDatos();
}
}


