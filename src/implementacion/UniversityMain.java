
package implementacion;


import controller.CourseController;
import controller.DepartmentController;
import controller.UniversityController;
import model.tableModel.CourseTableModel;
import model.tableModel.DepartmentTableModel;
import view.CourseView;
import view.DepartmentView;
import controller.ClassroomController;
import model.tableModel.ClassRoomTableModel;
import view.ClassRoomView;
import view.University;

public class UniversityMain {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		
		University view = new University();
		UniversityController controller = new UniversityController(view);
	
		view.setVisible(true);
	}
}
 