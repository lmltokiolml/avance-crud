package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class DepartmentDelete extends JFrame {

	private JPanel contentPane;
	private JButton btnRecuperar;
	private JLabel lblPresupuesto;
	private JLabel lblEdificio;
	private JLabel lblDeptName;
	private JScrollPane scrollPane;
	private JTextField txtDeptName;
	private JTextField txtEdificio;
	private JTextField txtPresupuesto;
	private JButton btnVolver;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DepartmentDelete frame = new DepartmentDelete();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DepartmentDelete() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Eliminados");
		lblNewLabel.setBounds(93, 11, 230, 34);
		contentPane.add(lblNewLabel);
		
		lblDeptName = new JLabel("Nombre");
		lblDeptName.setBounds(48, 58, 58, 14);
		contentPane.add(lblDeptName);
		
		lblEdificio = new JLabel("Edificio");
		lblEdificio.setBounds(58, 83, 58, 14);
		contentPane.add(lblEdificio);
		
		lblPresupuesto = new JLabel("Presupuesto");
		lblPresupuesto.setBounds(33, 114, 81, 14);
		contentPane.add(lblPresupuesto);
		
		btnRecuperar = new JButton("Recuperar");
		btnRecuperar.setBounds(290, 54, 104, 23);
		contentPane.add(btnRecuperar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(48, 143, 346, 125);
		contentPane.add(scrollPane);
		
		txtDeptName = new JTextField();
		txtDeptName.setBounds(116, 55, 164, 20);
		contentPane.add(txtDeptName);
		txtDeptName.setColumns(10);
		
		txtEdificio = new JTextField();
		txtEdificio.setColumns(10);
		txtEdificio.setBounds(116, 83, 164, 20);
		contentPane.add(txtEdificio);
		
		txtPresupuesto = new JTextField();
		txtPresupuesto.setColumns(10);
		txtPresupuesto.setBounds(116, 112, 164, 20);
		contentPane.add(txtPresupuesto);
		
		btnVolver = new JButton("Volver");
		btnVolver.setBounds(290, 94, 104, 23);
		contentPane.add(btnVolver);
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTextField getTxtDeptName() {
		return txtDeptName;
	}

	public void setTxtDeptName(JTextField txtDeptName) {
		this.txtDeptName = txtDeptName;
	}

	public JTextField getTxtEdificio() {
		return txtEdificio;
	}

	public void setTxtEdificio(JTextField txtEdificio) {
		this.txtEdificio = txtEdificio;
	}

	public JTextField getTxtPresupuesto() {
		return txtPresupuesto;
	}

	public void setTxtPresupuesto(JTextField txtPresupuesto) {
		this.txtPresupuesto = txtPresupuesto;
	}

	public JButton getBtnRecuperar() {
		return btnRecuperar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}
	
	

}
