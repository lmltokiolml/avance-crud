package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;

public class University extends JFrame {

	private JPanel contentPane;
	private JButton btnClassroom;
	private JButton btnCursos;
	private JButton btnDepartamentos;
	private JButton btnProfesores;
	private JButton btnSecciones;
	private JButton btnEstudiantes;
	private JButton btnTakes;
	private JButton btnTimeslot;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					University frame = new University();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public University() {
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 609, 274);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel("Sistema Universitario");
		lblTitulo.setFont(new Font("Arial Black", Font.BOLD, 15));
		lblTitulo.setBounds(175, 21, 208, 38);
		contentPane.add(lblTitulo);
		
	
		
		btnCursos = new JButton("Cursos");
		btnCursos.setFont(new Font("Arial Black", Font.PLAIN, 11));
		btnCursos.setBounds(143, 104, 107, 23);
		contentPane.add(btnCursos);
		
		btnDepartamentos = new JButton("Departamentos");
		btnDepartamentos.setFont(new Font("Arial Black", Font.PLAIN, 11));
		btnDepartamentos.setBounds(276, 104, 149, 23);
		contentPane.add(btnDepartamentos);
		
		JLabel lblEscojaUnaOpcion = new JLabel("escoja una opcion");
		lblEscojaUnaOpcion.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblEscojaUnaOpcion.setBounds(198, 59, 142, 23);
		contentPane.add(lblEscojaUnaOpcion);
		
		btnClassroom = new JButton("Salones");
		btnClassroom.setFont(new Font("Arial Black", Font.PLAIN, 11));
		btnClassroom.setBounds(226, 160, 107, 23);
		contentPane.add(btnClassroom);

		
	}



	public JButton getBtnCursos() {
		return btnCursos;
	}

	public JButton getBtnDepartamentos() {
		return btnDepartamentos;
	}

	public JButton getBtnClassroom() {
		return btnClassroom;
	}
}
