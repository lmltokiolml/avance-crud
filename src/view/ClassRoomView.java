package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;

public class ClassRoomView extends JFrame {

	private JPanel contentPane;
	private JTextField txtBulding;
	private JTextField txtCapacity;
	private JTextField txtBuscador;
	private JScrollPane scrollPane;
	private JComboBox comboBox;
	private JTextField txtnum;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClassRoomView frame = new ClassRoomView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClassRoomView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 404);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtBulding = new JTextField();
		txtBulding.setBounds(152, 74, 112, 20);
		contentPane.add(txtBulding);
		txtBulding.setColumns(10);
		
		txtCapacity = new JTextField();
		txtCapacity.setBounds(152, 136, 112, 20);
		contentPane.add(txtCapacity);
		txtCapacity.setColumns(10);
		
		JLabel lblSalonDeClases = new JLabel("salon de clases");
		lblSalonDeClases.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblSalonDeClases.setBounds(156, 21, 135, 20);
		contentPane.add(lblSalonDeClases);
		
		JLabel lblIdBulding = new JLabel("Edificio");
		lblIdBulding.setBounds(40, 77, 58, 14);
		contentPane.add(lblIdBulding);
		
		JLabel lblIdRoom = new JLabel("numero de salon");
		lblIdRoom.setBounds(32, 108, 89, 14);
		contentPane.add(lblIdRoom);
		
		JLabel lblIdCapacity = new JLabel("Capacidad");
		lblIdCapacity.setBounds(42, 139, 66, 14);
		contentPane.add(lblIdCapacity);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(299, 73, 89, 23);
		contentPane.add(btnGuardar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(299, 104, 89, 23);
		contentPane.add(btnModificar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(299, 135, 89, 23);
		contentPane.add(btnEliminar);
		
		txtBuscador = new JTextField();
		txtBuscador.setBounds(152, 167, 112, 20);
		contentPane.add(txtBuscador);
		txtBuscador.setColumns(10);
		
		JLabel lblBuscador = new JLabel("buscador");
		lblBuscador.setBounds(41, 170, 57, 14);
		contentPane.add(lblBuscador);
		
		JButton btnVerEliminados = new JButton("ver eliminados");
		btnVerEliminados.setBounds(299, 169, 101, 23);
		contentPane.add(btnVerEliminados);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 212, 348, 121);
		contentPane.add(scrollPane);
		
		txtnum = new JTextField();
		txtnum.setColumns(10);
		txtnum.setBounds(152, 105, 112, 20);
		contentPane.add(txtnum);
	}

	public JTextField getTxtbulding() {
		return txtBulding;
	}

	public void setTxtBulding(JTextField txtBulding) {
		this.txtBulding = txtBulding;
	}
	
	public JTextField getTxtCapacity() {
		return txtCapacity;
	}

	public void setTxtTitle(JTextField txtTitle) {
		this.txtCapacity = txtCapacity;
	}



	public JTextField getTxtBuscador() {
		return txtBuscador;
	}

	public void setTxtBuscador(JTextField txtBuscador) {
		this.txtBuscador = txtBuscador;
	}

	public JScrollPane getScrollPane() {
		return scrollPane();
	}

	private JScrollPane scrollPane() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JComboBox getComboBox() {
		return getComboBox();
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JButton getBtnGuardar() {
		return getBtnGuardar();
	}

	public JButton getBtnModificar() {
		return getBtnModificar();
	}

	public JButton getBtnEliminar() {
		return getBtnEliminar();
	}

	public JButton getBtnEliminados() {
		return getBtnEliminados();
	}
}
