package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class DepartmentView extends JFrame {

	private JPanel contentPane;
	private JTextField txtDeptName;
	private JTextField txtEdificio;
	private JTextField txtPresupuesto;
	private JTextField txtBuscador;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;
	private JButton btnEliminados;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DepartmentView frame = new DepartmentView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DepartmentView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Departamentos");
		lblNewLabel.setBounds(71, 11, 305, 49);
		contentPane.add(lblNewLabel);
		
		txtDeptName = new JTextField();
		txtDeptName.setBounds(133, 82, 148, 20);
		contentPane.add(txtDeptName);
		txtDeptName.setColumns(10);
		
		txtEdificio = new JTextField();
		txtEdificio.setColumns(10);
		txtEdificio.setBounds(133, 113, 148, 20);
		contentPane.add(txtEdificio);
		
		txtPresupuesto = new JTextField();
		txtPresupuesto.setColumns(10);
		txtPresupuesto.setBounds(133, 146, 148, 20);
		contentPane.add(txtPresupuesto);
		
		txtBuscador = new JTextField();
		txtBuscador.setColumns(10);
		txtBuscador.setBounds(133, 193, 148, 20);
		contentPane.add(txtBuscador);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(310, 81, 89, 23);
		contentPane.add(btnGuardar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(310, 112, 89, 23);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(310, 145, 89, 23);
		contentPane.add(btnEliminar);
		
		JLabel lblDeptName = new JLabel("Nombre");
		lblDeptName.setBounds(77, 85, 46, 14);
		contentPane.add(lblDeptName);
		
		JLabel lblEdificio = new JLabel("Edificio");
		lblEdificio.setBounds(71, 116, 52, 14);
		contentPane.add(lblEdificio);
		
		JLabel lblPresupuesto = new JLabel("Presupuesto");
		lblPresupuesto.setBounds(50, 149, 83, 14);
		contentPane.add(lblPresupuesto);
		
		JLabel lblBuscador = new JLabel("Buscador");
		lblBuscador.setBounds(65, 196, 58, 14);
		contentPane.add(lblBuscador);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(65, 236, 272, 119);
		contentPane.add(scrollPane);
		
		btnEliminados = new JButton("Ver Eliminados");
		btnEliminados.setBounds(291, 192, 133, 23);
		contentPane.add(btnEliminados);
	}

	public JTextField getTxtDeptName() {
		return txtDeptName;
	}

	public void setTxtDeptName(JTextField txtDeptName) {
		this.txtDeptName = txtDeptName;
	}

	public JTextField getTxtEdificio() {
		return txtEdificio;
	}

	public void setTxtEdificio(JTextField txtEdificio) {
		this.txtEdificio = txtEdificio;
	}

	public JTextField getTxtPresupuesto() {
		return txtPresupuesto;
	}

	public void setTxtPresupuesto(JTextField txtPresupuesto) {
		this.txtPresupuesto = txtPresupuesto;
	}

	public JTextField getTxtBuscador() {
		return txtBuscador;
	}

	public void setTxtBuscador(JTextField txtBuscador) {
		this.txtBuscador = txtBuscador;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public JButton getBtnEliminados() {
		return btnEliminados;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

}
