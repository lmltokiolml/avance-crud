package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

public class CourseDelete extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JButton btnVolver;
	private JButton btnRecuperar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CourseDelete frame = new CourseDelete();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CourseDelete() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 341);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Eliminados");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel.setBounds(98, 22, 230, 23);
		contentPane.add(lblNewLabel);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 74, 372, 154);
		contentPane.add(scrollPane);
		
		btnRecuperar = new JButton("Recuperar");
		btnRecuperar.setBounds(67, 254, 97, 23);
		contentPane.add(btnRecuperar);
		
		btnVolver = new JButton("Volver");
		btnVolver.setBounds(231, 254, 97, 23);
		contentPane.add(btnVolver);
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public JButton getBtnRecuperar() {
		return btnRecuperar;
	}
	
	
}
