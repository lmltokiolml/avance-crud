package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClassRoomDelete extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JButton btnRecuperar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClassRoomDelete frame = new ClassRoomDelete();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClassRoomDelete() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(115, 162, 119, -35);
		contentPane.add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(37, 47, 347, 115);
		contentPane.add(scrollPane_1);
		
		JLabel lblEliminador = new JLabel("eliminador");
		lblEliminador.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblEliminador.setBounds(136, 22, 119, 14);
		contentPane.add(lblEliminador);
		
		JButton btnRecuperar = new JButton("Recuperar");
		btnRecuperar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRecuperar.setBounds(65, 189, 89, 23);
		contentPane.add(btnRecuperar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(241, 189, 89, 23);
		contentPane.add(btnVolver);
	}
	public JScrollPane getScrollPane() {
		return getScrollPane();
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JButton getBtnVolver() {
		return getBtnVolver();
	}

	public JButton getBtnRecuperar() {
		return btnRecuperar;
	}
	

}
