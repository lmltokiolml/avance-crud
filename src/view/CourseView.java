package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.entities.Department;

import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CourseView extends JFrame {

	private JPanel contentPane;
	private JTextField txtCursoID;
	private JTextField txtTitle;
	private JTextField txtCreditos;
	private JTextField txtBuscador;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;
	private JButton btnEliminados;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CourseView frame = new CourseView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CourseView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 493);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cursos");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel.setBounds(172, 40, 89, 20);
		contentPane.add(lblNewLabel);
		
		txtCursoID = new JTextField();
		txtCursoID.setBounds(133, 82, 148, 20);
		contentPane.add(txtCursoID);
		txtCursoID.setColumns(10);
		
		txtTitle = new JTextField();
		txtTitle.setColumns(10);
		txtTitle.setBounds(133, 113, 148, 20);
		contentPane.add(txtTitle);
		
		txtCreditos = new JTextField();
		txtCreditos.setColumns(10);
		txtCreditos.setBounds(133, 177, 148, 20);
		contentPane.add(txtCreditos);
		
		txtBuscador = new JTextField();
		txtBuscador.setColumns(10);
		txtBuscador.setBounds(133, 220, 148, 20);
		contentPane.add(txtBuscador);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(309, 94, 89, 23);
		contentPane.add(btnGuardar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnModificar.setBounds(309, 125, 89, 23);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(309, 157, 89, 23);
		contentPane.add(btnEliminar);
		
		JLabel lblIdCurso = new JLabel("ID Curso");
		lblIdCurso.setBounds(71, 85, 52, 14);
		contentPane.add(lblIdCurso);
		
		JLabel lblTitle = new JLabel("Nombre");
		lblTitle.setBounds(71, 116, 52, 14);
		contentPane.add(lblTitle);
		
		JLabel lblPresupuesto = new JLabel("Creditos");
		lblPresupuesto.setBounds(80, 175, 83, 14);
		contentPane.add(lblPresupuesto);
		
		JLabel lblBuscador = new JLabel("Buscador");
		lblBuscador.setBounds(65, 223, 58, 14);
		contentPane.add(lblBuscador);
		
		JLabel lblDepartamento = new JLabel("Departamento");
		lblDepartamento.setBounds(50, 148, 100, 14);
		contentPane.add(lblDepartamento);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(65, 263, 272, 119);
		contentPane.add(scrollPane);
		
		btnEliminados = new JButton("Ver Eliminados");
		btnEliminados.setBounds(291, 219, 133, 23);
		contentPane.add(btnEliminados);
		
		comboBox = new JComboBox();
		List<Department> departments = new ArrayList<Department>();
		System.out.println(departments);
		comboBox.setBounds(133, 144, 148, 22);
		contentPane.add(comboBox);
	}

	public JTextField getTxtCursoID() {
		return txtCursoID;
	}

	public void setTxtCursoID(JTextField txtCursoID) {
		this.txtCursoID = txtCursoID;
	}
	
	public JTextField getTxtTitle() {
		return txtTitle;
	}

	public void setTxtTitle(JTextField txtTitle) {
		this.txtTitle = txtTitle;
	}

	public JTextField getTxtCreditos() {
		return txtCreditos;
	}

	public void setTxtCreditos(JTextField txtCreditos) {
		this.txtCreditos = txtCreditos;
	}

	public JTextField getTxtBuscador() {
		return txtBuscador;
	}

	public void setTxtBuscador(JTextField txtBuscador) {
		this.txtBuscador = txtBuscador;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public JButton getBtnEliminados() {
		return btnEliminados;
	}
	
	
}
